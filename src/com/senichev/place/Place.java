package com.senichev.place;

import javax.swing.*;
import java.awt.*;

/**
 * The "flow-interface" wrapper of java.swing.GridBagLayout.
 * <p/>
 * How to use: suppose we have a button which we want to put to the panel. So we do the following:
 * <pre>
 * {@code Place p = Place.of(button).row(0).column(0).topInset(10);
 *    p.at(panel);}
 * </pre>
 * This is the same as:
 * <pre>{@code GridBagConstraints c = new GridBagConstraints;
 * c.gridx = 0;
 * c.gridy = 0;
 * c.insets = new Insets(10,0,0,0);
 * panel.add(button,c);}</pre>
 * <p/>
 * The above example with Place can be rewritten in other way:
 * <pre>{@code Place p = Place.of(button);
 * p.row(0);
 * p.column(0);
 * p.topInset(10);
 * p.at(panel);}</pre>
 * <p/>
 * Any combination of methods can be grouped in one line. It's important, that Place be initialized by
 * static factory method  {@code of} and, after constraints have been set, method {@code at} should be called to
 * complete layout:
 * <pre>{@code Place p = Place.of(button);
 * p.row(0).column(0);
 * p.topInset(10).bottomInset(10).anchorEast();
 * p.rowspan(2);
 * p.at(panel);}</pre>
 * <p/>
 * The container (the panel in our example) should have GridBagLayout set, otherwise the exception is thrown by
 * method {@code at}.
 *
 *  @author Aleksey Senichev
 */
@SuppressWarnings("UnusedDeclaration")
public class Place {

    private final GridBagConstraints constraints;
    private final JComponent component;
    private int leftInset;
    private int rightInset;
    private int topInset;
    private int bottomInset;

    /**
     * Initializes Place object and sets the component to layout.
     * @param component - the component to layout
     * @return  - the Place object
     */
    public static Place of(JComponent component) {
        return new Place(component);
    }

    private Place(JComponent component) {
        this.component = component;
        this.constraints = new GridBagConstraints();
    }

    /**
     * Sets the index of a column, counting from 0, to add component to.
     * If column is not specified, the component will be placed immediately following
     * the component that was added to the container just before this component was added.
     * @param column - column number to layout component at. Must be non-negative.
     * @return - the Place object
     */
    public Place column(int column) {
        constraints.gridx = column;
        return this;
    }

    /**
     * Sets the index of a row, counting from 0, to add component to.
     * If row is not specified, the component will be placed immediately below
     * the component that was added to the
     * container just before this component was added.
     * @param row - row number to layout component at. Must be non-negative.
     * @return - the Place object
     */
    public Place row(int row) {
        constraints.gridy = row;
        return this;
    }

    /**
     * Sets the left inset
     * @param inset - the space that a container must leave at its left edge
     * @return  - Place object
     */
    public Place leftInset(int inset) {
        leftInset = inset;
        return this;
    }

    /**
     * Sets the right inset
     * @param inset - the space that a container must leave at its right edge
     * @return - Place object
     */
    public Place rightInset(int inset) {
        rightInset = inset;
        return this;
    }

    /**
     * Sets the top inset
     * @param inset - the space that a container must leave at its top edge
     * @return - Place object
     */
    public Place topInset(int inset) {
        topInset = inset;
        return this;
    }

    /**
     * Sets the bottom inset
     * @param inset - the space that a container must leave at its bottom edge
     * @return - the Place object
     */
    public Place bottomInset(int inset) {
        bottomInset = inset;
        return this;
    }

    /**
     * Puts the component on the right side of its display area, centered vertically.
     * @return - Place object
     */
    public Place anchorEast() {
        constraints.anchor = GridBagConstraints.EAST;
        return this;
    }

    /**
     * Puts the component on the left side of its display area, centered vertically.
     * @return - the Place object
     */
    public Place anchorWest() {
        constraints.anchor = GridBagConstraints.WEST;
        return this;
    }

    /**
     * Puts the component at the top of its display area, centered horizontally.
     * @return - the Place object
     */
    public Place anchorNorth() {
        constraints.anchor = GridBagConstraints.NORTH;
        return this;
    }

    /**
     * Put the component at the bottom of its display area, centered horizontally
     * @return - the Place object
     */
    public Place anchorSouth() {
        constraints.anchor = GridBagConstraints.SOUTH;
        return this;
    }

    /**
     * Puts the component at the top-left corner of its display area.
     * @return - the Place object
     */
    public Place anchorNorthWest() {
        constraints.anchor = GridBagConstraints.NORTHWEST;
        return this;
    }

    /**
     * Puts the component at the top-right corner of its display area.
     * @return - the Place object
     */
    public Place anchorNorthEast() {
        constraints.anchor = GridBagConstraints.NORTHEAST;
        return this;
    }

    /**
     * Put the component at the bottom-left corner of its display area.
     * @return - the Place object
     */
    public Place anchorSouthWest() {
        constraints.anchor = GridBagConstraints.SOUTHWEST;
        return this;
    }

    /**
     * Puts the component at the bottom-right corner of its display area.
     * @return - the Place object
     */
    public Place anchorSouthEast() {
        constraints.anchor = GridBagConstraints.SOUTHEAST;
        return this;
    }

    /**
     * Sets the number of columns to span
     * @param columns - number of columns to span
     * @return - the Place object
     */
    public Place colspan(int columns) {
        constraints.gridwidth=columns;
        return this;
    }

    /**
     * Sets the number of rows to span
     * @param rows - number of rows to span
     * @return - the Place object
     */
    public Place rowspan(int rows) {
        constraints.gridheight=rows;
        return this;
    }

    /**
     * Sets the width of the component equal to the cell width (method is used when a cell is larger than component)
     * @return - the Place object
     */
    public Place fillVertically() {
        constraints.fill=GridBagConstraints.VERTICAL;
        return this;
    }

    /**
     * Stretches the compSets the height of the component equal to the cell height (method is used when a cell is
     * larger than
     * component)
     * @return - the Place object
     */
    public Place fillHorizontally() {
        constraints.fill=GridBagConstraints.HORIZONTAL;
        return this;
    }

    /**
     * Stretches both dimensions of the component up to the cell's size
     * @return - the Place object
     */
    public Place fillBoth() {
        constraints.fill = GridBagConstraints.BOTH;
        return this;
    }

    /**
     * Distributes the extra space between rows in proportion to their weights.
    * A row that has a weight of zero receives no extra space.
    * @param weight - weight of a row. Must be non-negative.
    * @return  - the Place object
    */
    public Place verticalWeight(double weight) {
        constraints.weighty = weight;
        return this;
    }

    /**
     * Distributes the extra space between columns in proportion to their weights.
     * A column that has a weight of zero receives no extra space.
     * @param weight - weight of a column. Must be non-negative.
     * @return  - the Place object
     */
    public Place horizontalWeight(double weight) {
        constraints.weightx=weight;
        return this;
    }

    /**
     * Sets how much space to add to the minimum width of
     * the component. The width of the component is at least its minimum width plus number of pixels specified by
     * this method.
     * @param value - value of internal horizontal padding of the component
     * @return - the Place object
     */
    public Place internalPaddingX(int value) {
        constraints.ipadx = value;
        return this;
    }

    /**
     * Sets how much space to add to the minimum height of
     * the component. The height of the component is at least its minimum height plus number of pixels specified
     * by this method.
     * @param value - value of internal vertical padding of the component
     * @return - the Place object
     */
    public Place internalPaddingY(int value) {
        constraints.ipady = value;
        return this;
    }

    /**
     * Adds component to the container.
     * @param container - the container to layout at.
     * @throws IllegalStateException - if containers' layout is other than GridBagLayout.
     */
    public void at(JComponent container) {
        if (!(container.getLayout() instanceof GridBagLayout)) {
            throw new IllegalStateException("Wrong layout "
                    + container.getLayout());
        }
        setInsets();
        container.add(component, constraints);
    }

    private void setInsets() {
        constraints.insets =  new Insets(topInset, leftInset, bottomInset, rightInset);
    }
}
